# Description
  
Takes as argument an Executable and Linking Formatted (ELF) binary and  
parses the header.  
  
This program is roughly an equivalent to the Linux command readelf -h.

# Usage
  
```
chmod +x elfhdr_parser
elfhdr_parser <binary>
```

