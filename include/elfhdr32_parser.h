#ifndef _ELFHDR_PARSER_
#define _ELFHDR_PARSER_

#include <stdint.h>
#include <file_handler.h>

#define eprintf(FMT, ...) \
    fprintf(stderr, "%s:%d: ", __FILE__, __LINE__); \
    fprintf(stderr, FMT, __VA_ARGS__)

/* General purpose macros */
#define TRUE    1
#define FALSE   0
#define SIXTEEN_BITS    2 /* in bytes */
#define THIRTYTWO_BITS  4 /* in bytes */
#define SIXTYFOUR_BITS  8 /* in bytes */

/* ELF Identification */
/* These are the initial bytes of the file and specify how to interpret
   the file.  This enables an object file framework supporting multiple
   processors, multiple data encodings, and multiple classes of machines. */
#define EI_NIDENT   16 /* Size of e_ident[] */
#define EI_MAG0     0 /* File identification */
#define EI_MAG1     1 /* File identification */
#define EI_MAG2     2 /* File identification */
#define EI_MAG3     3 /* File identification */
#define EI_CLASS    4 /* File class */
#define EI_DATA     5 /* Data encoding */
#define EI_VERSION  6 /* File version */
#define PAD         7 /* Start of padding bytes */

/* Files first four bytes are the Magic Number which identify file as
   ELF object file */
#define ELFMAG0     0x7f /* e_ident[EI_MAG0] */
#define ELFMAG1     'E'  /* e_ident[EI_MAG1] */
#define ELFMAG2     'L'  /* e_ident[EI_MAG2] */
#define ELFMAG3     'F'  /* e_ident[EI_MAG3] */

/* Identifes files class or capacity */
#define ELFCLASSNONE    0 /* Invalid class */
#define ELFCLASS32      1 /* 32-bit objects */
#define ELFCLASS64      2 /* 64-bit objects */

/* Files data encoding, endianness */
#define ELFDATANONE     0 /* Invalid data encoding */
#define ELFDATA2LSB     1 /* 2's complement little endian */
#define ELFDATA2MSB     2 /* 2's complement big endian */

/* Object file types */
#define ET_NONE     0 /* No file */
#define ET_REL      1 /* Relocatable file */
#define ET_EXEC     2 /* Executable file */
#define ET_DYN      3 /* Shared Object file */
#define ET_CORE     4 /* Core file */
#define ET_LOPROC   0xff00 /* Processor-specific */
#define ET_HIPROC   0xffff /* Processor-specific */

/* Object file architectures */
#define EM_NONE     0 /* No machine architecture */
#define EM_M32      1 /* AT&T WE 32100 */
#define EM_SPARC    2 /* SPARC */
#define EM_386      3 /* Intel 80386 */
#define EM_68K      4 /* Motorola 68000 */
#define EM_88K      5 /* Motorola 88000 */
#define EM_860      7 /* Intel 80860 */
#define EM_MIPS     8 /* MIPS RS3000 */

/* Object file versions */
#define EV_NONE     0 /* Invalid */
#define EV_CURRENT  1 /* Current */

/* Special Section Indexes */
#define SHN_UNDEF       0
#define SHN_LORESERVE   0xff00
#define SHN_LOPROC      0xff00
#define SHN_HIPROC      0xff1f
#define SHN_ABS         0xfff1
#define SHN_COMMON      0xfff2
#define SHN_HIRESERVE   0xffff

typedef uint32_t Elf32_Addr;  /* Unsigned program address */
typedef uint16_t Elf32_Half;  /* Unsigned meidum integer */
typedef uint32_t Elf32_Off;   /* Unsigned file offset */
typedef int Elf32_Sword;      /* Signged large integer */
typedef uint32_t Elf32_Word;  /* Usigned large integer */

typedef struct
{
    unsigned char e_ident[EI_NIDENT]; /* TODO: add comment */
    Elf32_Half e_type;               /* Identifies type of object file */
    Elf32_Half e_machine;            /* Required architecture */
    Elf32_Word e_version;            /* Identifies object file version */
    Elf32_Addr e_entry;              /* Virtual address starting point */
    Elf32_Off  e_phoff;              /* Program header offset in bytes */
    Elf32_Off  e_shoff;              /* Section header offset in bytes */
    Elf32_Word e_flags;              /* Processor specific flags */
    Elf32_Half e_ehsize;             /* Elf header size in bytes */
    Elf32_Half e_phentsize;          /* Program header entry size */
    Elf32_Half e_phnum;              /* Number of entries in program header */
    Elf32_Half e_shentsize;          /* Section header size in bytes */
    Elf32_Half e_shnum;              /* Number of entries in section header */
    Elf32_Half e_shstrndx;           /* Section header table index of name */
} Elf32_Ehdr;

// TODO: add doxygen comments
Elf32_Ehdr* parse_elfhdr32(file_buffer_t buffer);
// TODO: add doxygen comments
void print_elfhdr32(Elf32_Ehdr *hdr);

#endif /* _ELFHDR_PARSER_ */
