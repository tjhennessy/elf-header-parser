#ifndef _FILE_HANDLER_H
#define _FILE_HANDLER_H

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

typedef uint8_t *file_buffer_t;
typedef long file_size_t;

/**
 * @brief Takes as an argument a file path and returns the size in bytes of
 * the file.
 * @param filename Is path and name of file which size will be determined for.
 * @return On success returns non-negative size, on failure returns negative value.
 */
file_size_t get_file_size(FILE *fp);

/**
 * @brief Opens a file in binary mode for and reads bytes into a buffer.
 * 
 * @params filename Is path and name of file which will be read into the buffer.
 * @return On success returns a pointer to a buffer of bytes, on failure
 * returns NULL.
 */
file_buffer_t get_file_buffer(char *filename);

/**
 * @brief Primarily a debugging tool for printing out the bytes in a buffer.
 */
void print_file_bytes(file_buffer_t buff, int n);

#endif /* _FILE_HANDLER_H */