#include <stdio.h>
#include <stdlib.h>
#include <endian.h>
#include <errno.h>
#include <string.h>
#include <elfhdr32_parser.h>

/**
 * TODO: add comments
 */
static void free_ehdr(Elf32_Ehdr **hdr)
{
    if (*hdr != NULL)
    {
#ifndef NDEBUG
    eprintf("%s\n", "Freeing header");
#endif
        free(*hdr);
        *hdr = NULL;
    }
}

/**
 * TODO: add comments
 */
static void free_buffer(file_buffer_t *buffer)
{
    if (*buffer != NULL)
    {
#ifndef NDEBUG
    eprintf("%s\n", "Freeing buffer");
#endif
        free(*buffer);
        *buffer = NULL;
    }
}

/**
 * @brief Ensures the destination bytes are formatted according to 
 * to the host architecture.
 * 
 * @param dest Stores bytes according to host byte order.
 * @param src Bytes which are copied and converted into host byte order.
 * @param byte_order Determines host byte order.
 */
static void handle_endian16(uint16_t *dest, uint16_t src, uint8_t byte_order)
{
    switch (byte_order)
    {   // to little endian
        case ELFDATA2LSB:
            *dest = htole16(src);
        break;
        // to big endian
        case ELFDATA2MSB:
            *dest = htobe16(src);
        break;
        default:
        break;
    }
}

/**
 * @brief Ensures the destination bytes are formatted according to 
 * to the host architecture.
 * 
 * @param dest Stores bytes according to host byte order.
 * @param src Bytes which are copied and converted into host byte order.
 * @param byte_order Determines host byte order.
 */
static void handle_endian32(uint32_t *dest, uint32_t src, uint8_t byte_order)
{
    switch (byte_order)
    {   // to little endian
        case ELFDATA2LSB:
            *dest = htole32(src);
        break;
        // to big endian
        case ELFDATA2MSB:
            *dest = htobe32(src);
        break;
        default:
        break;
    }
}

/**
 * @brief Ensures the destination bytes are formatted according to 
 * to the host architecture.
 * 
 * @param dest Stores bytes according to host byte order.
 * @param src Bytes which are copied and converted into host byte order.
 * @param byte_order Determines host byte order.
 */
static void handle_endian64(uint64_t *dest, uint64_t src, uint8_t byte_order)
{
    switch (byte_order)
    {   // to little endian
        case ELFDATA2LSB:
            *dest = htole64(src);
        break;
        // to big endian
        case ELFDATA2MSB:
            *dest = htobe64(src);
        break;
        default:
        break;
    }
}

/**
 * @brief Copies into header unsigned 16-bit integer value which is the
 * number of section headers in the section header table.
 * 
 * The product of e_shentsize and e_phnum is the table size in bytes.
 *
 * @param hdr 32-bit ELF formatted struct.
 * @param buffer Holds contents of ELF binary file
 * @param offset Pointer to an integer which represents offset into buffer.
 */
static void parse_eshstrndx(Elf32_Ehdr *hdr, file_buffer_t buffer, int *offset)
{
    memcpy(&(hdr->e_shstrndx), (buffer + *offset), sizeof(hdr->e_shstrndx));
    handle_endian16(&(hdr->e_shstrndx), hdr->e_shstrndx, hdr->e_ident[EI_DATA]);

#ifndef NDEBUG
    if (SHN_UNDEF == hdr->e_shstrndx)
    {
        eprintf("%s\n", "No section name string table exists");
    }
    else
    {
        eprintf("Section name string table index is %hu\n", hdr->e_shstrndx);
    }
#endif
}

/**
 * @brief Copies into header unsigned 16-bit integer value which is the
 * number of section headers in the section header table.
 * 
 * The product of e_shentsize and e_phnum is the table size in bytes.
 *
 * @param hdr 32-bit ELF formatted struct.
 * @param buffer Holds contents of ELF binary file
 * @param offset Pointer to an integer which represents offset into buffer.
 */
static void parse_eshnum(Elf32_Ehdr *hdr, file_buffer_t buffer, int *offset)
{
    memcpy(&(hdr->e_shnum), (buffer + *offset), sizeof(hdr->e_shnum));
    handle_endian16(&(hdr->e_shnum), hdr->e_shnum, hdr->e_ident[EI_DATA]);

#ifndef NDEBUG
    eprintf("Number of section headers is %hu\n", hdr->e_shnum);
#endif
}

/**
 * @brief Copies into header unsigned 16-bit integer value which is the
 * size in bytes of the section headers.
 *
 * @param hdr 32-bit ELF formatted struct.
 * @param buffer Holds contents of ELF binary file
 * @param offset Pointer to an integer which represents offset into buffer.
 */
static void parse_eshentsize(Elf32_Ehdr *hdr, file_buffer_t buffer, int *offset)
{
    memcpy(&(hdr->e_shentsize), (buffer + *offset), sizeof(hdr->e_shentsize));
    handle_endian16(&(hdr->e_shentsize), hdr->e_shentsize, hdr->e_ident[EI_DATA]);

#ifndef NDEBUG
    eprintf("Size of section headers is %hu\n", hdr->e_shentsize);
#endif
}

/**
 * @brief Copies into header unsigned 16-bit integer value which is the
 * number of entries in the program header table.
 * 
 * The product of e_phentsize and e_phnum is the actual table size in bytes.
 * 
 * @param hdr 32-bit ELF formatted struct.
 * @param buffer Holds contents of ELF binary file
 * @param offset Pointer to an integer which represents offset into buffer.
 */
static void parse_ephnum(Elf32_Ehdr *hdr, file_buffer_t buffer, int *offset)
{
    memcpy(&(hdr->e_phnum), (buffer + *offset), sizeof(hdr->e_phnum));
    handle_endian16(&(hdr->e_phnum), hdr->e_phnum, hdr->e_ident[EI_DATA]);

#ifndef NDEBUG
    eprintf("Number of program headers is %hu\n", hdr->e_phnum);
#endif
}

/**
 * @brief Copies into header unsigned 16-bit integer value which is the
 * size in bytes of program headers.
 *
 * @param hdr 32-bit ELF formatted struct.
 * @param buffer Holds contents of ELF binary file
 * @param offset Pointer to an integer which represents offset into buffer.
 */
static void parse_ephentsize(Elf32_Ehdr *hdr, file_buffer_t buffer, int *offset)
{
    memcpy(&(hdr->e_phentsize), (buffer + *offset), sizeof(hdr->e_phentsize));
    handle_endian16(&(hdr->e_phentsize), hdr->e_phentsize, hdr->e_ident[EI_DATA]);

#ifndef NDEBUG
    eprintf("Size of program headers in bytes is %hu\n", hdr->e_phentsize);
#endif
}

/**
 * @brief Copies into header unsigned 16-bit integer value which is the
 * size of the ELF header in bytes.
 *
 * @param hdr 32-bit ELF formatted struct.
 * @param buffer Holds contents of ELF binary file
 * @param offset Pointer to an integer which represents offset into buffer.
 */
static void parse_ehsize(Elf32_Ehdr *hdr, file_buffer_t buffer, int *offset)
{
    memcpy(&(hdr->e_ehsize), (buffer + *offset), sizeof(hdr->e_ehsize));

    switch (hdr->e_ident[EI_DATA])
    {
        case ELFDATANONE:
            // Just leave bytes as they are
        break;
        case ELFDATA2LSB:
            hdr->e_machine = htole16(hdr->e_machine);
        break;
        case ELFDATA2MSB:
            hdr->e_machine = htobe16(hdr->e_machine);
        break;
        default:
            // Unknown bytes read, leave as they are
        break;
    }

#ifndef NDEBUG
    eprintf("Header size is %hu bytes\n", hdr->e_ehsize);
#endif
}

/**
 * @brief Copies into header unsigned 32-bit integer value which is
 * consists of processor-specific flags.
 * 
 * The 32-bit Intel Architecture does not define any flags, as such this
 * value is zero for 32-bit Intel Architectures.
 *
 * @param hdr 32-bit ELF formatted struct.
 * @param buffer Holds contents of ELF binary file
 * @param offset Pointer to an integer which represents offset into buffer.
 */
static void parse_eflags(Elf32_Ehdr *hdr, file_buffer_t buffer, int *offset)
{
    memcpy(&(hdr->e_flags), (buffer + *offset), sizeof(hdr->e_flags));
    handle_endian32(&(hdr->e_flags), 
        hdr->e_flags, hdr->e_ident[EI_DATA]);

#ifndef NDEBUG
    eprintf("value for header e_flags is 0x%0X\n", hdr->e_flags);
#endif
}

/**
 * @brief Copies into header unsigned 32-bit integer value which is
 * the section header table's position within the buffer.
 *
 * @param hdr 32-bit ELF formatted struct.
 * @param buffer Holds contents of ELF binary file
 * @param offset Pointer to an integer which represents offset into buffer.
 */
static void parse_eshoff(Elf32_Ehdr *hdr, file_buffer_t buffer, int *offset)
{
    memcpy(&(hdr->e_shoff), (buffer + *offset), sizeof(hdr->e_shoff));
    handle_endian32(&(hdr->e_shoff), hdr->e_shoff, hdr->e_ident[EI_DATA]);

#ifndef NDEBUG
    eprintf("e_shoff is %d bytes into file\n", hdr->e_shoff);
#endif
}

/**
 * @brief Copies into header unsigned 32-bit integer value which is
 * the program header table's position within the buffer.
 *
 * @param hdr 32-bit ELF formatted struct.
 * @param buffer Holds contents of ELF binary file
 * @param offset Pointer to an integer which represents offset into buffer.
 */
static void parse_ephoff(Elf32_Ehdr *hdr, file_buffer_t buffer, int *offset)
{
    memcpy(&(hdr->e_phoff), (buffer + *offset), sizeof(hdr->e_phoff));
    handle_endian32(&(hdr->e_phoff), hdr->e_phoff, hdr->e_ident[EI_DATA]);

#ifndef NDEBUG
    eprintf("e_phoff is %d bytes into file\n", hdr->e_phoff);
#endif
}

/**
 * @brief Copies into header unsigned 32-bit integer value which is
 * the virtual address where execution starts.
 * 
 * @param hdr 32-bit ELF formatted struct.
 * @param buffer Holds contents of ELF binary file
 * @param offset Pointer to an integer which represents offset into buffer.
 */
static void parse_eentry(Elf32_Ehdr *hdr, file_buffer_t buffer, int *offset)
{
    memcpy(&(hdr->e_entry), (buffer + *offset), sizeof(hdr->e_entry));
    handle_endian32(&(hdr->e_entry), hdr->e_entry, hdr->e_ident[EI_DATA]);

#ifndef NDEBUG
    eprintf("e_entry value is 0x%0X\n", hdr->e_entry);
#endif
}

/**
 * @brief Copies into header unsigned 32-bit integer value into ELF header
 * structure.
 * 
 * @param hdr 32-bit ELF formatted struct.
 * @param buffer Holds contents of ELF binary file
 * @param offset Pointer to an integer which represents offset into buffer.
 */
static void parse_eversion(Elf32_Ehdr *hdr, file_buffer_t buffer, int *offset)
{
    memcpy(&(hdr->e_version), (buffer + *offset), sizeof(hdr->e_version));
    handle_endian32(&(hdr->e_version), hdr->e_version, hdr->e_ident[EI_DATA]);

#ifndef NDEBUG
    eprintf("e_version is %s\n", hdr->e_machine == EV_NONE ? "EV_NONE" : "EV_CURRENT");
#endif
}

/**
 * @brief Copies into header unsigned 32-bit integer value which is
 * the required architecture for the bytes in buffer.
 * 
 * @param hdr 32-bit ELF formatted struct.
 * @param buffer Holds contents of ELF binary file
 * @param offset Pointer to an integer which represents offset into buffer.
 */
static void parse_emachine(Elf32_Ehdr *hdr, file_buffer_t buffer, int *offset)
{
    memcpy(&(hdr->e_machine), (buffer + *offset), sizeof(hdr->e_machine));
    handle_endian16(&(hdr->e_machine), hdr->e_machine, hdr->e_ident[EI_DATA]);

#ifndef NDEBUG
    switch (hdr->e_machine)
    {
        case EM_NONE:
            eprintf("e_machine data: %s\n", "No machine");
        break;
        case EM_M32:
            eprintf("e_machine data: %s\n", "AT&T WE 32100");
        break;
        case EM_SPARC:
            eprintf("e_machine data: %s\n", "SPARC");
        break;
        case EM_386:
            eprintf("e_machine data: %s\n", "Intel 80386");
        break;
        case EM_68K:
            eprintf("e_machine data: %s\n", "Motorolla 68000");
        break;
        case EM_88K:
            eprintf("e_machine data: %s\n", "Motorolla 88000");
        break;
        case EM_860:
            eprintf("e_machine data: %s\n", "Intel 80860");
        break;
        case EM_MIPS:
            eprintf("e_machine data: %s\n", "MIPS RS3000");
        break;
        default:
            // format not found
        break;
    }
#endif
}

/**
 * @brief Copies into header unsigned 32-bit integer value which is
 * the object file type.
 * 
 * @param hdr 32-bit ELF formatted struct.
 * @param buffer Holds contents of ELF binary file
 * @param offset Pointer to an integer which represents offset into buffer.
 */
static void parse_etype(Elf32_Ehdr *hdr, file_buffer_t buffer, int *offset)
{
    memcpy(&(hdr->e_type), (buffer + *offset), sizeof(hdr->e_type));
    handle_endian16(&(hdr->e_type), hdr->e_type, hdr->e_ident[EI_DATA]);

#ifndef NDEBUG
    switch (hdr->e_type)
    {
        case ET_NONE:
            eprintf("%s\n", "ET_NONE ELF Type");
        break;
        case ET_REL:
            eprintf("%s\n", "ET_REL ELF Type");
        break;
        case ET_EXEC:
            eprintf("%s\n", "ET_EXEC ELF Type");
        break;
        case ET_DYN:
            eprintf("%s\n", "ET_DYN ELF Type");
        break;
        case ET_CORE: 
            eprintf("%s\n", "ET_CORE ELF Type");
        break;
        case ET_LOPROC: /* Processor-specific */
            eprintf("%s\n", "ET_LOPROC ELF Type");
        break;
        case ET_HIPROC: /* Processor-specific */
            eprintf("%s\n", "ET_HIPROC ELF Type");
        break;
        default:
        break;
    }
#endif
}

/**
 * @brief Copies into header unsigned 32-bit integer value which contains
 * machine independent data.
 * 
 * @param hdr 32-bit ELF formatted struct.
 * @param buffer Holds contents of ELF binary file
 * @param offset Pointer to an integer which represents offset into buffer.
 */
static void parse_eident(Elf32_Ehdr *hdr, file_buffer_t buffer)
{
    memcpy(hdr->e_ident, buffer, EI_NIDENT);

#ifndef NDEBUG
    eprintf("Magic number is %X\n", hdr->e_ident[EI_MAG0]);
    eprintf("%c%c%c\n",
        hdr->e_ident[EI_MAG1],
        hdr->e_ident[EI_MAG2],
        hdr->e_ident[EI_MAG3]);
    eprintf("Is 32-bit ELF object class type %d\n",
        ELFCLASS32 == hdr->e_ident[EI_CLASS]);
    eprintf("Data encoding type is %d\n", hdr->e_ident[EI_DATA]);
    switch (hdr->e_ident[EI_DATA])
    {
        case ELFDATANONE:
            eprintf("%s\n", "Invalid data encoding");
        break;
        case ELFDATA2LSB:
            eprintf("%s\n", "Little endian encoding");
        break;
        case ELFDATA2MSB:
            eprintf("%s\n", "Big endian encoding");
        break;
    }
#endif
}

Elf32_Ehdr* parse_elfhdr32(file_buffer_t buffer)
{
    Elf32_Ehdr *hdr = NULL;
    int retval;
    int offset; /* keeps track of position in buffer of ELF Header */

    hdr = calloc(1, sizeof(Elf32_Ehdr));
    if (NULL == hdr)
    {
        eprintf("Error: %s\n", "memory allocator failed");
        exit(EXIT_FAILURE);
    }

    parse_eident(hdr, buffer);

    offset = EI_NIDENT;
    parse_etype(hdr, buffer, &offset);

    offset += sizeof(hdr->e_type);
    parse_emachine(hdr, buffer, &offset);

    offset += sizeof(hdr->e_machine);
    parse_eversion(hdr, buffer, &offset);

    offset += sizeof(hdr->e_version);
    parse_eentry(hdr, buffer, &offset);

    offset += sizeof(hdr->e_entry);
    parse_ephoff(hdr, buffer, &offset);

    offset += sizeof(hdr->e_phoff);
    parse_eshoff(hdr, buffer, &offset);

    offset += sizeof(hdr->e_shoff);
    parse_eflags(hdr, buffer, &offset);

    offset += sizeof(hdr->e_flags);
    parse_ehsize(hdr, buffer, &offset);

    offset += sizeof(hdr->e_ehsize);
    parse_ephentsize(hdr, buffer, &offset);

    offset += sizeof(hdr->e_phentsize);
    parse_ephnum(hdr, buffer, &offset);

    offset += sizeof(hdr->e_phnum);
    parse_eshentsize(hdr, buffer, &offset);

    offset += sizeof(hdr->e_shentsize);
    parse_eshnum(hdr, buffer, &offset);

    offset += sizeof(hdr->e_shnum);
    parse_eshstrndx(hdr, buffer, &offset);

    return hdr;
}

// Expose in API
void print_elfhdr32(Elf32_Ehdr *hdr)
{
    printf("ELF Header:\n");

    printf("  Magic: %X %c%c%c\n",
        hdr->e_ident[EI_MAG0],
        hdr->e_ident[EI_MAG1],
        hdr->e_ident[EI_MAG2],
        hdr->e_ident[EI_MAG3]);

    printf("  Class: ELF32\n");

    printf("  Data: ");
    switch (hdr->e_ident[EI_DATA])
    {
        case ELFDATANONE:
            printf("Invalid data encoding\n");
        break;
        case ELFDATA2LSB:
            printf("2's complement, little endian\n");
        break;
        case ELFDATA2MSB:
            printf("2's complement, big endian\n");
        break;
    }

    printf("  Version: %s\n", 
        hdr->e_machine == EV_NONE ? "EV_NONE" : "EV_CURRENT");

    // TODO: figure out how to extract OS/ABI info from binary
    // e.g. OS/ABI: UNIX -System V

    // TODO: figure out how to extract ABI version

    printf("  Type: ");
    switch (hdr->e_type)
    {
        case ET_NONE:
            printf("NONE\n");
        break;
        case ET_REL:
            printf("REL\n");
        break;
        case ET_EXEC:
            printf("EXEC\n");
        break;
        case ET_DYN:
            printf("DYN (Shared object file)\n");
        break;
        case ET_CORE: 
            printf("CORE\n");
        break;
        case ET_LOPROC: /* Processor-specific */
            printf("LOPROC\n");
        break;
        case ET_HIPROC: /* Processor-specific */
            printf("HIPROC\n");
        break;
        default:
        break;
    }

    printf("  Machine: ");
    switch (hdr->e_machine)
    {
        case EM_NONE:
            printf("No machine\n");
        break;
        case EM_M32:
            printf("AT&T WE 32100\n");
        break;
        case EM_SPARC:
            printf("SPARC\n");
        break;
        case EM_386:
            printf("Intel 80386\n");
        break;
        case EM_68K:
            printf("Motorolla 68000\n");
        break;
        case EM_88K:
            printf("Motorolla 88000\n");
        break;
        case EM_860:
            printf("Intel 80860\n");
        break;
        case EM_MIPS:
            printf("MIPS RS3000\n");
        break;
        default:
            // format not found
        break;
    }

    printf("  Entry point address: 0x%0X\n", hdr->e_entry);

    printf("  Start of program headers: %hu (bytes)\n", hdr->e_phoff);

    printf("  Start of section headers: %hu (bytes into file)\n", hdr->e_shoff);

    printf("  Flags: 0x%X\n", hdr->e_flags);
    
    printf("  Size of this header: %hu (bytes)\n", hdr->e_ehsize);

    printf("  Size of program headers: %hu (bytes)\n", hdr->e_phentsize);

    printf("  Number of program headers: %hu\n", hdr->e_phnum);

    printf("  Size of section headers: %hu (bytes)\n", hdr->e_shentsize);

    printf("  Number of section headers: %hu\n", hdr->e_shnum);

    printf("  Section header string table index: %hu\n", hdr->e_shstrndx);
}

int main(int argc, char *argv[])
{
    int retval;
    file_buffer_t __attribute__ ((cleanup(free_buffer))) file_bytes = NULL;
    Elf32_Ehdr __attribute__ ((cleanup(free_ehdr))) *header = NULL;

    if (argc < 2)
    {
        fprintf(stderr, "usage: %s <elf file>\n", argv[0]);
        return 1;
    }

    file_bytes = get_file_buffer(argv[1]);
    if (NULL == file_bytes)
    {
        eprintf("Error: %s\n", "could not buffer file");
        return 1;
    }

    header = parse_elfhdr32(file_bytes);
    if (NULL == header)
    {
        eprintf("Error: %s\n", "could not parse ELF object file");
        return 1;
    }

    print_elfhdr32(header);

    return 0;
}
