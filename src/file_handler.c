#include <file_handler.h>

/**
 * @brief Private function for use with gcc cleanup attribute.
 */
static void close_file(FILE **fd)
{
    if (*fd != NULL)
    {
#ifndef NDEBUG
    printf("%s\n", "Closing file");
#endif
        fclose(*fd);
    }
}

file_size_t get_file_size(FILE *fp)
{
    long size;

    fseek(fp, 0L, SEEK_END);
    size = ftell(fp);
    rewind(fp);

#ifndef NDEBUG
    printf("size of file is %ld\n", size);
#endif

    return size;
}

file_buffer_t get_file_buffer(char *filename)
{
    size_t num_bytes;
    file_size_t fsize;
    file_buffer_t buffer = NULL;
    FILE __attribute__((cleanup(close_file))) *ifp = NULL;

#ifndef NDEBUG
    printf("%s%s\n", "parsing elf file -> ", filename);
#endif 

    ifp = fopen(filename, "rb");
    if (NULL == ifp)
    {
        printf("Error: %s\n", strerror(errno));
        return NULL;
    }

    fsize = get_file_size(ifp);
    if (fsize < 0)
    {
        printf("Error: could not determine file size: %s\n", strerror(errno));
        return NULL;
    }

    buffer = calloc(1, (size_t) fsize);
    if (NULL == buffer)
    {
        printf("Error: %s\n", "could not allocate memory");
        exit(EXIT_FAILURE);
    }

    num_bytes = fread(buffer, 1, fsize, ifp);
    if (num_bytes != fsize)
    {
        printf("Error: %s\n", "could not read file into buffer");
        return NULL;
    }

    return buffer;
}

void print_file_bytes(file_buffer_t buff, int n)
{
    int i;
    int j;
    int k;

    for (i = 0; i < n; i++)
    {
        printf("%02X ", buff[i]);

        if (i % 16 == 0 && i > 0)
        {
            printf("\n");
        }
    }
}